package com.google.android.ads.consent.prefs

import android.content.Context
import android.content.SharedPreferences
import com.google.android.ads.consent.ConsentStatus

class UserConsentPreferences {
    private var context: Context? = null
    private var preferences: SharedPreferences? = null
    private var dateSharedPreferences = "date_shared_preferences"
    private var isPersonalizedDatePrefKey = "is_personalized_date_pref_key"
    private var nonPersonalizedDatePrefKey = "non_personalized_date_pref_key"
    private val isUserAgreementPrefKey = "user_agreement_pref_key"

    var isPersonalized: Boolean = true
    var nonPersonalized: Boolean = false

    private val userPersonalizedPrefKey = "ads_preference"
    private val adContentRatingValuePreference = "ad_content_rating"
    private val californiaConsumerPrivacyActPreference = "ad_california_consumer_privacy_act_preference"
    private val userInformationPrefKey = "ads_information_preference"
    private val userLocationFromEeaStatusPrefkey = "user_status"

    private var editor: SharedPreferences.Editor? = null

    constructor(context: Context?){
        this.context = context
        preferences = context!!.getSharedPreferences(dateSharedPreferences, Context.MODE_PRIVATE)
        editor = preferences?.edit()
    }

    companion object {
        fun getInstance(context: Context?) : UserConsentPreferences {
            return UserConsentPreferences(context)
        }
    }

    fun isUserConfirmAgreement(): Boolean {
        return preferences!!.getBoolean(isUserAgreementPrefKey, false)
    }

    fun setUserConfirmAgreement(confirm: Boolean) {
        editor?.putBoolean(isUserAgreementPrefKey, confirm)
        editor?.apply()
    }


    fun addUserConsentInformation(value: String?) {
        editor!!.putString(userInformationPrefKey, value!!).apply()
    }

    fun getUserConsentInformation(): String {
        return preferences!!.getString(userInformationPrefKey, ConsentStatus.UNKNOWN.name).toString()
    }

    // UserConsent add status
    fun setUserConsentPersonalizedAdPref(value: Boolean?) {
        preferences!!.edit().putBoolean(userPersonalizedPrefKey, value!!).apply()
    }



    // Set The California Consumer Privacy Act (CCPA)
    fun setCaliforniaConsumerPrivacyAct(isCaliforniaConsumerPrivacyAct: Boolean) {
        editor!!.putBoolean(californiaConsumerPrivacyActPreference, isCaliforniaConsumerPrivacyAct).apply()
    }

    // Get The California Consumer Privacy Act (CCPA)
    fun isCaliforniaConsumerPrivacyAct(): Boolean {
        return preferences!!.getBoolean(californiaConsumerPrivacyActPreference, false)
    }

    // UserConsent add AdContentRatingValue status
    fun setAdContentRatingValuePref(value: String?) {
        preferences!!.edit().putString(adContentRatingValuePreference, value!!).apply()
    }

    // UserConsent get AdContentRatingValue status
    fun getAdContentRatingValuePref(): String {
        return preferences!!.getString(adContentRatingValuePreference, "").toString()
    }

    // UserConsent is status
    fun isUserConsentPersonalizedAd(): Boolean {
        return preferences!!.getBoolean(userPersonalizedPrefKey, isPersonalized)
    }

    // UserConsent is status of Personalized
    fun isUserConsentNonPersonalizedAd(): Boolean {
        return preferences!!.getBoolean(userPersonalizedPrefKey, nonPersonalized)
    }

    // UserConsent is within
    fun updateUserConsentPersonalized(value: Boolean?) {
        preferences!!.edit().putBoolean(userPersonalizedPrefKey, value!!).apply()
    }


    // Check the user location
    fun isUserLocationFromInEea(): Boolean {
        return preferences!!.getBoolean(userLocationFromEeaStatusPrefkey, false)
    }

    // setupShape user location
    fun setUserLocationFromInEea(value: Boolean) {
        return editor!!.putBoolean(userLocationFromEeaStatusPrefkey, value).apply()
    }

    // Update consent from settings
    fun setUserConsentPersonalizedDate(date: String){
        editor!!.putString(isPersonalizedDatePrefKey, date).apply()
    }

    fun setUserConsentNonPersonalizedDate(date: String){
        editor!!.putString(nonPersonalizedDatePrefKey, date).apply()
    }

    fun getUserConsentPersonalizedDate(): String{
        return preferences!!.getString(isPersonalizedDatePrefKey, "---").toString()
    }

    fun getUserConsentNonPersonalizedDate(): String{
        return preferences!!.getString(nonPersonalizedDatePrefKey, "---").toString()
    }
}