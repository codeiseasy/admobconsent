package com.google.android.ads.consent.util

import android.graphics.Color
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.widget.TextView
import android.text.style.URLSpan
import android.text.TextPaint


object ConsentTextSpannable {

    fun makeLink(textView: TextView?, links: Array<String>, clickableSpans: Array<ClickableSpan>) {
        textView!!.setHintTextColor(Color.TRANSPARENT)
        textView.movementMethod = LinkMovementMethod.getInstance()
        textView.setText(textView.text, TextView.BufferType.SPANNABLE)

        ConsentLogDebug.d("make_link", links.size.toString() + clickableSpans.size)
    }

    fun makeLinks(textView: TextView?, links: Array<String>, clickableSpans: Array<ClickableSpan>) {
        val spannableStrig = SpannableString(textView!!.text)
        for (i in links.indices) {
            val clickableSpan = clickableSpans[i]
            val link = links[i]
            val startIndexOfLink = textView.text.toString().indexOf(link)
            spannableStrig.setSpan(
                    clickableSpan,
                    startIndexOfLink,
                    startIndexOfLink + link.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        textView.setHintTextColor(Color.TRANSPARENT)
        textView.movementMethod = LinkMovementMethod.getInstance()
        textView.setText(spannableStrig, TextView.BufferType.SPANNABLE)
    }

    fun makeMultiLinks(textView: Array<TextView?>, links: Array<String>, clickableSpans: Array<ClickableSpan>) {
        for (i in textView.indices){
            val spannableStrig = SpannableString(textView[i]!!.text)
            for (x in links.indices) {
                val clickableSpan = clickableSpans[x]
                val link = links[x]
                val startIndexOfLink = textView[x]!!.text.toString().indexOf(link)
                spannableStrig.setSpan(
                        clickableSpan,
                        startIndexOfLink,
                        startIndexOfLink + link.length,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
            textView[i]?.setHintTextColor(Color.TRANSPARENT)
            textView[i]?.movementMethod = LinkMovementMethod.getInstance()
            textView[i]?.setText(spannableStrig, TextView.BufferType.SPANNABLE)
        }
    }

    fun stripUnderlines(textView: TextView) {
        val s = SpannableString(textView.text)
        val spans = s.getSpans(0, s.length, URLSpan::class.java)
        for (span in spans) {
            var onspan = span
            val start = s.getSpanStart(onspan)
            val end = s.getSpanEnd(onspan)
            s.removeSpan(onspan)
            onspan = URLSpanNoUnderline(onspan.url)
            s.setSpan(onspan, start, end, 0)
        }
        textView.text = s
    }

    private class URLSpanNoUnderline(url: String) : URLSpan(url) {
        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }
}