package com.google.android.ads.consent.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codeiseasy.rippleview.Rippley
import com.google.android.ads.consent.R
import com.google.android.ads.consent.models.AdProviders
import com.google.android.ads.consent.util.ConsentUtils
import com.google.android.ads.consent.views.ConsentDrawableVector

class AdMobProvidersAdapter(private val providers: MutableList<AdProviders>, private val accentColor: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private class MyHolder(view: View) : RecyclerView.ViewHolder(view) {
        val providerName: TextView = view.findViewById(R.id.provider_name)
        val providerLink: TextView = view.findViewById(R.id.provider_link)
        val providerParentRipple: Rippley = view.findViewById(R.id.provider_ripple_parent)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyHolder(LayoutInflater.from(parent.context).inflate(R.layout.user_consent_eea_provider_items, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val myHolder = holder as MyHolder
        val context = myHolder.itemView.context

        val provider = providers[position]

        myHolder.providerName.text = provider.getName()
        myHolder.providerLink.text = "View"
        myHolder.providerLink.setTextColor(accentColor)

        myHolder.providerParentRipple.setOnClickListener {
            ConsentUtils.openBrowser(context, provider.getLink())
        }
        myHolder.providerParentRipple.background = ConsentDrawableVector.setupBorderDrawable().setCornerRadius(5f).setStroke(2, accentColor).apply()
    }


    override fun getItemCount(): Int {
        return providers.size
    }
}