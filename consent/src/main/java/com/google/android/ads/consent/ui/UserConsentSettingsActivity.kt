package com.google.android.ads.consent.ui


import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.core.content.ContextCompat

import android.text.TextPaint
import android.text.style.ClickableSpan
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.recyclerview.widget.RecyclerView
import com.google.android.ads.consent.ConsentInformation
import com.google.android.ads.consent.ConsentStatus
import com.google.android.ads.consent.models.AdProviders
import com.google.android.ads.consent.prefs.UserConsentPreferences
import com.google.android.ads.consent.util.ConsentConstants
import com.google.android.ads.consent.util.ConsentTextSpannable
import com.google.android.ads.consent.util.ConsentUtils
import com.google.android.ads.consent.util.ConsentUtils.getCurrentDate
import com.google.android.ads.consent.R
import com.google.android.ads.consent.UserConsentSdk
import com.google.android.ads.consent.adapters.AdMobProvidersAdapter
import com.google.android.ads.consent.util.ConsentFontUtil
import com.google.android.ads.consent.views.ConsentCustomToolbar
import java.util.*

class UserConsentSettingsActivity : AppCompatActivity() {
    private var checkBoxPersonalizedAds: CheckBox? = null
    private var checkBoxNonPersonalizedAds: CheckBox? = null

    private var btnRevokeConsent: Button? = null

    private var tvDatePersonalizedAds: TextView? = null
    private var tvDateNonPersonalizedAds: TextView? = null

    private var tvTextPersonalizedAds: TextView? = null
    private var tvTextNonPersonalizedAds: TextView? = null

    private var userConsent: UserConsentSdk? = null
    private var userConsentPreferences: UserConsentPreferences? = null

    private var isAlreadyUpdated = false


    companion object {
        fun start(context: Context, privacyUrl: String, publisherId: String){
            context.startActivity(Intent(context, UserConsentSettingsActivity::class.java)
                    .putExtra(ConsentConstants.extraKeyPrivacyUrl, privacyUrl)
                    .putExtra(ConsentConstants.extraKeyPublisherId, publisherId)
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_consent_eea_activity_settings)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        val toolbar = findViewById<ConsentCustomToolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        if (supportActionBar != null){
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowTitleEnabled(true)
            supportActionBar?.title = resources.getString(R.string.user_consent_settings)
            ConsentUtils.deepChangeTextColor(this, toolbar, "fonts/Cocon-Next-Arabic.ttf")
        }

        val extras = intent.extras
        if (extras != null){
            val privacyUrl = extras.getString(ConsentConstants.extraKeyPrivacyUrl)
            val publisherId = extras.getString(ConsentConstants.extraKeyPublisherId)
            userConsent = UserConsentSdk.Builder()
                    .setContext(this)
                    .setPrivacyUrl(privacyUrl) // Add your privacy policy url
                    .setPublisherId(publisherId) // Add your admob publisher id
                    .setTestDeviceId(AdMobRequest.DEVICE_ID_EMULATOR) // Add your test device id "Remove addTestDeviceId on production!"
                    .setLog(UserConsentSettingsActivity::class.java.name) // Add custom tag default: ID_LOG
                    .setDebugGeography(true) // Geography appears as in EEA for test devices.
                    //.setTagForChildDirectedTreatment(true)
                    .setShowingWindowWithAnimation(true)
                    .build()

            checkBoxPersonalizedAds = findViewById(R.id.radio_btn_personalized_ads)
            checkBoxNonPersonalizedAds = findViewById(R.id.radio_btn_non_personalized_ads)

            btnRevokeConsent = findViewById(R.id.btn_revoke_consent)

            tvDatePersonalizedAds = findViewById(R.id.tv_date_personalized_ads)
            tvDateNonPersonalizedAds = findViewById(R.id.tv_date_non_personalized_ads)

            tvTextPersonalizedAds = findViewById(R.id.tv_text_personalized_ads)
            tvTextNonPersonalizedAds = findViewById(R.id.tv_text_non_personalized_ads)

            val providerText = resources.getString(R.string.user_consent_personal_ads_link)
            val privacyLink = String.format(resources.getString(R.string.user_consent_non_personal_ads_link), resources.getString(R.string.user_consent_privacy_policy))

            //var privacyText = String.format(tvTextNonPersonalizedAds!!.text.toString(), privacyLink)

            tvTextPersonalizedAds?.text = String.format(resources.getString(R.string.user_consent_personal_ads_text), providerText)
            tvTextNonPersonalizedAds?.text = String.format(tvTextNonPersonalizedAds!!.text.toString(), privacyLink)

            val providerClickSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                        providerDialog()
                }
                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.isUnderlineText = false
                    ds.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14f, resources.displayMetrics)
                    if (tvTextPersonalizedAds!!.isPressed &&
                            tvTextPersonalizedAds!!.selectionStart !=  -1 &&
                            tvTextPersonalizedAds!!.text.toString().substring(tvTextPersonalizedAds!!.selectionStart, tvTextPersonalizedAds!!.selectionEnd) == providerText
                    ){
                        ds.color = Color.parseColor("#2E409F")
                    } else {
                        ds.color = Color.parseColor("#2781D2")
                    }
                }
            }
            val privacyClickSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                    ConsentUtils.openBrowser(this@UserConsentSettingsActivity, privacyUrl)
                }
                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.isUnderlineText = false
                    ds.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14f, resources.displayMetrics)
                    if (tvTextNonPersonalizedAds!!.isPressed &&
                            tvTextNonPersonalizedAds?.selectionStart !=  -1 &&
                            tvTextNonPersonalizedAds?.text.toString().substring(tvTextNonPersonalizedAds!!.selectionStart, tvTextNonPersonalizedAds!!.selectionEnd) == privacyLink
                    ){
                        ds.color = Color.parseColor("#2E409F")
                    } else {
                        ds.color = Color.parseColor("#2781D2")
                    }
                }
            }
            ConsentTextSpannable.makeLinks(tvTextPersonalizedAds!!, arrayOf(providerText), arrayOf(providerClickSpan))
            ConsentTextSpannable.makeLinks(tvTextNonPersonalizedAds!!, arrayOf(privacyLink), arrayOf(privacyClickSpan))

            if (userConsent!!.userConsentPreferences.getUserConsentInformation() == ConsentStatus.PERSONALIZED.toString()){
                checkBoxPersonalizedAds?.isChecked = true
                checkBoxNonPersonalizedAds?.isChecked = false
            } else {
                checkBoxPersonalizedAds?.isChecked = false
                checkBoxNonPersonalizedAds?.isChecked = true
            }
            checkBoxPersonalizedAds?.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked){
                    checkBoxNonPersonalizedAds?.isChecked = false
                }
                setEnableBtnRevokeConsent()
            }
            checkBoxNonPersonalizedAds?.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked){
                    checkBoxPersonalizedAds?.isChecked = false
                }
                setEnableBtnRevokeConsent()
            }
            btnRevokeConsent?.setOnClickListener{
                confirmDialog()
            }
            userConsentPreferences = UserConsentPreferences.getInstance(this)
            updateCurrentDate()
        }
    }

    private fun updateCurrentDate(){
        tvDatePersonalizedAds?.text = String.format(
                resources.getString(R.string.user_consent_personal_ads_date),
                userConsentPreferences?.getUserConsentPersonalizedDate()
        )
        tvDateNonPersonalizedAds?.text = String.format(
                resources.getString(R.string.user_consent_personal_ads_date),
                userConsentPreferences?.getUserConsentNonPersonalizedDate()
        )
    }

    private fun setEnableBtnRevokeConsent(){
        if (!btnRevokeConsent!!.isEnabled) {
            btnRevokeConsent?.isEnabled = true
        }
    }

    private fun providerDialog(){
        val dialog = Dialog(this, R.style.user_consent_DialogTheme)
        val layoutProvider = LayoutInflater.from(this).inflate(R.layout.user_consent_eea_partners, null)
        //val providerTitle = layoutProvider.findViewById<TextView>(R.id.provider_title)
        val providerIcon = layoutProvider.findViewById<ImageView>(R.id.provider_icon)
        //val providerLabel = layoutProvider.findViewById<TextView>(R.id.provider_label)
        //val providerPrivacy = layoutProvider.findViewById<TextView>(R.id.provider_privacy)
        val providerList = layoutProvider.findViewById<RecyclerView>(R.id.provider_list)
        val providerImgBack = layoutProvider.findViewById<ImageView>(R.id.provider_img_back)


        //providerTitle.visibility = View.GONE
        providerIcon.visibility = View.GONE
        //providerPrivacy.visibility = View.GONE

        val drawable = ContextCompat.getDrawable(this, R.drawable.user_consent_ic_close_white_24dp)
        providerImgBack.setImageDrawable(drawable)
        providerImgBack.setColorFilter(Color.DKGRAY)
        //providerImgBack.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)

        providerImgBack.setOnClickListener {
            dialog.dismiss()
        }

        val listProvider = ArrayList<AdProviders>()
        val adProviders = ConsentInformation.getInstance(this).adProviders
        for (adProvider in adProviders) {
            listProvider.add(AdProviders(adProvider.name, adProvider.privacyPolicyUrlString))
        }
        //var providerAdapter = AdProviderAdapter(listProvider, Color.RED)
        //providerList.adapter = providerAdapter

        val providerAdapter = AdMobProvidersAdapter(listProvider, Color.RED)
        providerList.adapter = providerAdapter

        ConsentFontUtil.overrideFonts(this, layoutProvider, "Roboto-Regular.ttf")
        dialog.setContentView(layoutProvider)
        dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // dialog.window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        dialog.setCancelable(true)
        dialog.show()
    }


    private fun confirmDialog(){
        val alertDialog = AlertDialog.Builder(this)
                .setMessage("Are you sure to want update consent?")
                .setNegativeButton("No") { dialog, _ ->
                    dialog?.cancel()
                }
                .setPositiveButton("Yes") { dialog, _ ->
                    if (checkBoxPersonalizedAds!!.isChecked){
                        userConsent?.updateUserConsent(ConsentStatus.PERSONALIZED)
                        userConsentPreferences!!.setUserConsentPersonalizedDate(getCurrentDate())
                        updateCurrentDate()
                        isAlreadyUpdated = true
                    } else if (checkBoxNonPersonalizedAds!!.isChecked){
                        userConsent?.updateUserConsent(ConsentStatus.NON_PERSONALIZED)
                        userConsentPreferences!!.setUserConsentNonPersonalizedDate(getCurrentDate())
                        updateCurrentDate()
                        isAlreadyUpdated = false
                    }
                    Handler(Looper.getMainLooper()).postDelayed({
                        btnRevokeConsent?.isEnabled = false
                    }, 500)
                    dialog?.dismiss()
                }

        val dialog = alertDialog.create()
        if (userConsent!!.isShowingWindowWithAnimation){
            dialog.window!!.attributes.windowAnimations = R.style.user_consent_DialogAnimation
        }
        dialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
