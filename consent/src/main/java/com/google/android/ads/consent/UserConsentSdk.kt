package com.google.android.ads.consent

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatDialog
import androidx.recyclerview.widget.RecyclerView
import com.codeiseasy.rippleview.Rippley
import com.google.ads.mediation.admob.AdMobAdapter
import com.google.android.ads.consent.adapters.AdMobProvidersAdapter
import com.google.android.ads.consent.enums.WindowMode
import com.google.android.ads.consent.listener.abstracts.UserConsentInformationCallback
import com.google.android.ads.consent.listener.enums.AdContentRating
import com.google.android.ads.consent.listener.interfaces.ConsentInfoUpdateListener
import com.google.android.ads.consent.listener.interfaces.UserConsentEventListener
import com.google.android.ads.consent.models.AdProviders
import com.google.android.ads.consent.prefs.UserConsentPreferences
import com.google.android.ads.consent.ui.UserConsentDialogStyle
import com.google.android.ads.consent.util.*
import com.google.android.ads.consent.views.ConsentDrawableVector
import com.google.android.gms.ads.AdRequest
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.util.*

class UserConsentSdk(private var context: Context?) {
    private val handler = Handler(Looper.getMainLooper())
    private var windowMode: WindowMode? = null
    private var dialog: AppCompatDialog? = null

    var isDebugGeography: Boolean = false
    var isShowingWindowWithAnimation: Boolean = false

    var privacyUrl: String? = null
    var publisherId: String? = null

    var testDeviceId: String? = ""
    var logTag: String? = ""

    var userConsentDialogStyle: UserConsentDialogStyle
    var userConsentPreferences: UserConsentPreferences = UserConsentPreferences.getInstance(this.context)

    init {
        this.userConsentDialogStyle = UserConsentDialogStyle(this.context)
    }

    fun build(): UserConsentSdk {
        return this
    }

    // Builder class
    class Builder {
        private var ctx: Context? = null
        private var isCaliforniaConsumerPrivacyAct: Boolean = false
        private var logTag: String? = "ID_LOG"
        private var deviceId: String? = ""
        private var privacyUrl: String? = null
        private var publisherId: String? = null
        private var maxAdContentRating: AdContentRating? = null
        private var isDebugGeography: Boolean = false
        private var isWindowAnimation: Boolean = false
        private var userConsent: UserConsentSdk? = null
        private var windowMode: WindowMode? = null
        private var userConsentDialogStyle: UserConsentDialogStyle? = null

        // Initialize Builder
        fun setContext(ctx: Context?): Builder {
            this.ctx = ctx
            return this
        }

        // Initialize The California Consumer Privacy Act (CCPA)
        fun setCaliforniaConsumerPrivacyAct(isCaliforniaConsumerPrivacyAct: Boolean): Builder {
            this.isCaliforniaConsumerPrivacyAct = isCaliforniaConsumerPrivacyAct
            return this
        }

        //@Deprecated("Test Device Id is deprecated from this version")
        fun setTestDeviceId(id: String?): Builder {
            this.deviceId = id
            return this
        }

        // Add privacy policy
        fun setPrivacyUrl(url: String?): Builder {
            this.privacyUrl = url
            return this
        }

        // Add Publisher Id
        fun setPublisherId(id: String?): Builder {
            this.publisherId = id
            return this
        }

        fun setDebugGeography(debug: Boolean): Builder {
            this.isDebugGeography = debug
            return this
        }

        // Add Logcat id
        fun setLog(log: String?): Builder {
            this.logTag = log
            return this
        }

        fun setUserConsentStyle(dialogDialogStyle: UserConsentDialogStyle): Builder {
            this.userConsentDialogStyle = dialogDialogStyle
            return this
        }

        // setupShape animation
        fun setShowingWindowWithAnimation(anim: Boolean): Builder {
            this.isWindowAnimation = anim
            return this
        }

        fun setShowingWindowMode(mode: WindowMode): Builder {
            this.windowMode = mode
            return this
        }

        // Build
        fun build(): UserConsentSdk {

            userConsent = UserConsentSdk(ctx)
            userConsent?.privacyUrl = privacyUrl.toString()
            userConsent?.publisherId = publisherId.toString()
            userConsent?.testDeviceId = deviceId.toString()
            userConsent?.isDebugGeography = isDebugGeography
            userConsent?.logTag = logTag.toString()
            userConsent?.setShowingWindowMode(windowMode)
            userConsent?.isShowingWindowWithAnimation = isWindowAnimation
            if (userConsentDialogStyle != null) {
                userConsent?.userConsentDialogStyle = userConsentDialogStyle!!
            }
            userConsent!!.userConsentPreferences.setAdContentRatingValuePref(maxAdContentRating.toString())
            userConsent!!.userConsentPreferences.setCaliforniaConsumerPrivacyAct(isCaliforniaConsumerPrivacyAct)

            return userConsent!!.build()
        }
    }

    // UserConsent add MaxAdContentRating status
    fun setShowingWindowMode(mode: WindowMode?) {
        windowMode = mode
    }

    // Get AdRequest
    fun getAdMobRequest(): AdRequest {
        ConsentLogDebug.d("isCaliforniaConsumerPrivacyAct", userConsentPreferences.isCaliforniaConsumerPrivacyAct().toString())
        ConsentLogDebug.d("PERSONALIZATION", userConsentPreferences.isUserConsentNonPersonalizedAd().toString())
        val networkExtrasBundle = Bundle()
        if (userConsentPreferences.isUserConsentNonPersonalizedAd()) {
            networkExtrasBundle.putString("npa", "1")
            if (userConsentPreferences.isCaliforniaConsumerPrivacyAct()) {
                networkExtrasBundle.putInt("rdp", 1)
            }
            return AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter::class.java, networkExtrasBundle)
                .build()
        }

        if (userConsentPreferences.isCaliforniaConsumerPrivacyAct()) {
            val networkExtrasBundleRdp = Bundle()
            networkExtrasBundleRdp.putInt("rdp", 1)
            return AdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter::class.java, networkExtrasBundleRdp)
                .build()
        }

        return AdRequest.Builder().build()
    }

    // ConsentManager information
    private fun initUserConsentInformation(callback: UserConsentInformationCallback?) {
        val consentInformation = ConsentInformation.getInstance(context)
        if (isDebugGeography) {
            ConsentLogDebug.d("initUserConsentInformation", "$isDebugGeography \nEID: $testDeviceId")
            consentInformation.addTestDevice(testDeviceId)
            consentInformation.debugGeography = DebugGeography.DEBUG_GEOGRAPHY_EEA
        }
        val publisherIds = arrayOf(publisherId)
        consentInformation.requestConsentInfoUpdate(publisherIds, object :
            ConsentInfoUpdateListener {
            override fun onConsentInfoUpdated(consentStatus: ConsentStatus) {
                callback?.onResult(consentInformation, consentStatus)
            }

            override fun onFailedToUpdateConsentInfo(reason: String) {
                callback?.onFailed(consentInformation, reason)
            }
        })
    }

    fun checkUserConsent(userConsentEventListener: UserConsentEventListener?) {
        when {
            userConsentPreferences.getUserConsentInformation() == ConsentStatus.UNKNOWN.name -> {
                initUserConsentInformation(object : UserConsentInformationCallback() {
                    override fun onResult(consentInformation: ConsentInformation, consentStatus: ConsentStatus) {
                        when (consentStatus) {
                            ConsentStatus.UNKNOWN -> {
                                if (consentInformation.isRequestLocationInEeaOrUnknown) {
                                    userConsentDialog(userConsentEventListener)
                                    userConsentPreferences.setUserLocationFromInEea(true)
                                } else {
                                    userConsentPreferences.addUserConsentInformation(ConsentStatus.PERSONALIZED.name)
                                    userConsentPreferences.setUserConsentPersonalizedAdPref(userConsentPreferences.isPersonalized)
                                    userConsentPreferences.setUserLocationFromInEea(false)
                                    userConsentPreferences.setUserConsentPersonalizedDate(ConsentUtils.getCurrentDate())
                                    userConsentEventListener?.onResult(ConsentStatus.PERSONALIZED, false)
                                }
                            }
                            else -> {
                                userConsentPreferences.addUserConsentInformation(ConsentStatus.PERSONALIZED.name)
                                userConsentPreferences.setUserConsentPersonalizedAdPref(userConsentPreferences.isPersonalized)
                                userConsentPreferences.setUserLocationFromInEea(false)
                                userConsentPreferences.setUserConsentPersonalizedDate(ConsentUtils.getCurrentDate())
                                userConsentEventListener?.onResult(ConsentStatus.PERSONALIZED, false)
                            }
                        }
                    }

                    override fun onFailed(consentInformation: ConsentInformation, reason: String) {
                        userConsentEventListener?.onFailed(reason)
                    }
                })
            }
            userConsentPreferences.getUserConsentInformation() == ConsentStatus.PERSONALIZED.name -> {
                userConsentEventListener?.onResult(ConsentStatus.PERSONALIZED, true)
            }
            userConsentPreferences.getUserConsentInformation() == ConsentStatus.NON_PERSONALIZED.name -> {
                userConsentEventListener?.onResult(ConsentStatus.NON_PERSONALIZED, false)
            }
        }

    }

    fun userConsentDialog(userConsentEventListener: UserConsentEventListener?) {
        val rootView = LinearLayout(context)
        rootView.orientation = LinearLayout.VERTICAL
        val rootViewParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        val layoutEuConsent = LayoutInflater.from(context).inflate(R.layout.user_consent_eea_body_main, null)
        val euConsentLinearContainer =
            layoutEuConsent.findViewById<LinearLayout>(R.id.ll_eu_consent_container)
        val euConsentTopLinear = layoutEuConsent.findViewById<LinearLayout>(R.id.eu_consent_top_ll)
        val euConsentBodyParent =
            layoutEuConsent.findViewById<LinearLayout>(R.id.consent_body_parent)
        val euConsentTitle = layoutEuConsent.findViewById<TextView>(R.id.eu_consent_title)
        val euConsentIcon = layoutEuConsent.findViewById<ImageView>(R.id.eu_consent_icon)
        val euConsentBodyText = layoutEuConsent.findViewById<TextView>(R.id.eu_consent_text)
        val euConsentQuestion = layoutEuConsent.findViewById<TextView>(R.id.eu_consent_question)
        val euConsentChangeSetting =
            layoutEuConsent.findViewById<TextView>(R.id.eu_consent_change_setting)
        val euConsentBtnPersonalizedAgree =
            layoutEuConsent.findViewById<Button>(R.id.eu_consent_personalized_agree)
        val euConsentBtnNoThanks =
            layoutEuConsent.findViewById<Button>(R.id.btn_eu_consent_disagree)
        val euConsentBtnRemoveAds =
            layoutEuConsent.findViewById<Button>(R.id.btn_eu_consent_remove_ads)

        val layoutProvider =
            LayoutInflater.from(context).inflate(R.layout.user_consent_eea_partners, null)
        val providerContainerLinear =
            layoutProvider.findViewById<LinearLayout>(R.id.provider_container_ll)
        val providerTopLinear = layoutProvider.findViewById<LinearLayout>(R.id.provider_top_ll)
        val providerBodyParent = layoutProvider.findViewById<LinearLayout>(R.id.provider_body_parent)
        val providerBottomLinear =
            layoutProvider.findViewById<Rippley>(R.id.provider_bottom_ll)
        val providerTitle = layoutProvider.findViewById<TextView>(R.id.provider_title)
        val providerIcon = layoutProvider.findViewById<ImageView>(R.id.provider_icon)
        //val providerLabel = layoutProvider.findViewById<TextView>(R.id.provider_label)
        val providerPrivacy = layoutProvider.findViewById<TextView>(R.id.provider_privacy)
        //var providerList = layoutProvider.findViewById<ListView>(R.id.provider_list)
        val providerList = layoutProvider.findViewById<RecyclerView>(R.id.provider_list)
        val providerImgBack = layoutProvider.findViewById<ImageView>(R.id.provider_img_back)

        val layoutDisagree =
            LayoutInflater.from(context).inflate(R.layout.user_consent_eea_disagree, null)
        val disagreeContainerLinear =
            layoutDisagree.findViewById<LinearLayout>(R.id.disagree_container_ll)
        val disagreeTopLinear = layoutDisagree.findViewById<LinearLayout>(R.id.disagree_top_ll)
        val disagreeBodyParent =
            layoutDisagree.findViewById<LinearLayout>(R.id.disagree_body_parent)
        val disagreeBottomLinear =
            layoutDisagree.findViewById<Rippley>(R.id.disagree_bottom_ll)
        val disagreeTitle = layoutDisagree.findViewById<TextView>(R.id.disagree_title)
        val disagreeIcon = layoutDisagree.findViewById<ImageView>(R.id.disagree_icon)
        val disagreeText = layoutDisagree.findViewById<TextView>(R.id.disagree_text)
        val disagreePrivacy = layoutDisagree.findViewById<TextView>(R.id.disagree_privacy)
        val disagreeBtnNonPersonalizedAgree =
            layoutDisagree.findViewById<Button>(R.id.disagree_btn_non_personalized_agree)
        val disagreeImgBack = layoutDisagree.findViewById<ImageView>(R.id.disagree_img_back)

        euConsentTitle.text = getAppName(context)
        providerTitle.text = getAppName(context)
        disagreeTitle.text = getAppName(context)

        euConsentBtnPersonalizedAgree.setOnClickListener(
            UserConsentButtonsListener(
                userConsentEventListener
            )
        )
        disagreeBtnNonPersonalizedAgree.setOnClickListener(
            UserConsentButtonsListener(
                userConsentEventListener
            )
        )
        euConsentBtnRemoveAds.setOnClickListener(UserConsentButtonsListener(userConsentEventListener))

        val listProvider = ArrayList<AdProviders>()
        val adProviders = ConsentInformation.getInstance(context).adProviders
        for (adProvider in adProviders) {
            listProvider.add(AdProviders(adProvider.name, adProvider.privacyPolicyUrlString))
        }
        //var providerAdapter = AdProviderAdapter(listProvider, userConsentDialogStyle?.getAccentColor())
        // providerList.adapter = providerAdapter

        val providerAdapter = AdMobProvidersAdapter(listProvider, userConsentDialogStyle.getAccentColor())
        providerList.adapter = providerAdapter

        euConsentBodyText.text = ResourcesCompat.getString(context, R.string.user_consent_eu_consent_text)
        euConsentQuestion.text = ResourcesCompat.getString(context, R.string.user_consent_eu_consent_question)

        val locale = Locale.ENGLISH
        val disagreeChangeSettingText = String.format(locale, ResourcesCompat.getString(context, R.string.user_consent_eu_consent_disagree_text), getAppName(context))
        disagreeText.text = disagreeChangeSettingText

        val euConsentChangeSettingText = String.format(
            locale,
            ResourcesCompat.getString(context, R.string.user_consent_eea_consent_change_setting),
            getAppName(context)
        )
        val euConsentChangeSettingLearnMoreText = String.format(locale,
            ResourcesCompat.getString(context, R.string.user_consent_learn_more),
            getAppName(context)
        )
        euConsentChangeSetting.text = (euConsentChangeSettingText + "\n" + euConsentChangeSettingLearnMoreText)

        val dataIsUsed = euConsentChangeSettingLearnMoreText
        //euConsentChangeSetting!!.setLinkTextColor(Color.RED)
        val dataIsUsedClickSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                //dialog?.setContentView(layoutProvider)
                rootView.removeAllViews().also {
                    rootView.post {
                        rootView.addView(layoutProvider, rootViewParams)
                    }
                }
            }
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
                ds.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, userConsentDialogStyle.getPrivacyTextSize(), context!!.resources.displayMetrics)
                if (euConsentChangeSetting.isPressed &&
                    euConsentChangeSetting.selectionStart != -1 &&
                    euConsentChangeSetting.text.toString().substring(
                        euConsentChangeSetting.selectionStart,
                        euConsentChangeSetting.selectionEnd
                    ) == dataIsUsed
                ) {
                    ds.color = userConsentDialogStyle.getBodyTextColor()
                } else {
                    ds.color = userConsentDialogStyle.getPrivacyTextColor()
                }
            }
        }
        val privacyLink = String.format(ResourcesCompat.getString(context, R.string.user_consent_how_uses_data), getAppName(context))

        providerPrivacy.setText(privacyLink, TextView.BufferType.SPANNABLE)
        disagreePrivacy.setText(privacyLink, TextView.BufferType.SPANNABLE)

        providerBottomLinear.setOnClickListener { ConsentUtils.openBrowser(context, privacyUrl) }
        providerPrivacy.setOnClickListener { ConsentUtils.openBrowser(context, privacyUrl) }
        disagreeBottomLinear.setOnClickListener { ConsentUtils.openBrowser(context, privacyUrl) }
        disagreePrivacy.setOnClickListener { ConsentUtils.openBrowser(context, privacyUrl) }

        val titleParents = arrayListOf(euConsentTitle, providerTitle, disagreeTitle)
        for (titleParent in titleParents) {
            ConsentFontUtil.overrideColors(
                context,
                titleParent,
                userConsentDialogStyle.getTopBarTitleColor()
            )
            ConsentFontUtil.overrideFonts(
                context,
                titleParent,
                userConsentDialogStyle.getTopBarTitleFont()
            )
            ConsentFontUtil.overrideSizes(
                context,
                titleParent,
                userConsentDialogStyle.getTopBarTitleSize()
            )
        }

        val privacyParents = arrayListOf(providerPrivacy, disagreePrivacy)
        for (privacyParent in privacyParents) {
            ConsentFontUtil.overrideColors(
                context,
                privacyParent,
                userConsentDialogStyle.getPrivacyTextColor()
            )
            ConsentFontUtil.overrideFonts(
                context,
                privacyParent,
                userConsentDialogStyle.getPrivacyTextFont()
            )
            ConsentFontUtil.overrideSizes(
                context,
                privacyParent,
                userConsentDialogStyle.getPrivacyTextSize()
            )
        }

        val bodyParents = arrayListOf(euConsentBodyParent, providerBodyParent, disagreeBodyParent)
        for (bodyParent in bodyParents) {
            ConsentFontUtil.overrideColors(context, bodyParent, userConsentDialogStyle.getBodyTextColor())
            ConsentFontUtil.overrideFonts(context, bodyParent, userConsentDialogStyle.getBodyTextFont())
            ConsentFontUtil.overrideSizes(context, bodyParent, userConsentDialogStyle.getBodyTextSize())
        }

        euConsentLinearContainer.background = ConsentDrawableVector.setupShape(context)
            .backgroundColor(userConsentDialogStyle.getBackgroundColor())
            .cornerRadius(userConsentDialogStyle.getAllCornerRadius())
            .apply()

        providerContainerLinear.background = ConsentDrawableVector.setupShape(context)
            .backgroundColor(userConsentDialogStyle.getBackgroundColor())
            .cornerRadius(userConsentDialogStyle.getAllCornerRadius())
            .apply()

        disagreeContainerLinear.background = ConsentDrawableVector.setupShape(context)
            .backgroundColor(userConsentDialogStyle.getBackgroundColor())
            .cornerRadius(userConsentDialogStyle.getAllCornerRadius())
            .apply()

        // TODO top of dialog
        val layoutParents = arrayListOf(euConsentTopLinear, disagreeTopLinear, providerTopLinear)
        for (layoutParent in layoutParents) {
            if (userConsentDialogStyle.isMultipleColors()) {
                layoutParent.background = ConsentDrawableVector.setupShape(context)
                    .backgroundColors(userConsentDialogStyle.getTopBarBackgroundColors())
                    .cornerRadiusTop(userConsentDialogStyle.getAllCornerRadius())
                    .apply()
            } else {
                layoutParent.background = ConsentDrawableVector.setupShape(context)
                    .backgroundColor(userConsentDialogStyle.getTopBarBackgroundColor())
                    .cornerRadiusTop(userConsentDialogStyle.getAllCornerRadius())
                    .apply()
            }
        }

        // Image btn back change color
        //DrawableCompat.setTint(providerImgBack.drawable, Color.parseColor(Utils.setColorAlpha(userConsentDialogStyle?.getDialogImgBackColor(), 80)))
        //DrawableCompat.setTint(disagreeImgBack.drawable, Color.parseColor(Utils.setColorAlpha(userConsentDialogStyle?.getDialogImgBackColor(), 80)))

        ConsentLogDebug.d("HEX_COLOR", userConsentDialogStyle.getButtonBackColor().toString())
        val colorHex =
            "#" + Integer.toHexString(userConsentDialogStyle.getTopBarTitleColor() and 0x00ffffff)
        providerImgBack.setColorFilter(Color.parseColor(ConsentUtils.setColorAlpha(colorHex, 95)))
        disagreeImgBack.setColorFilter(Color.parseColor(ConsentUtils.setColorAlpha(colorHex, 95)))

        providerBottomLinear.background = ConsentDrawableVector.setupShape(context)
            .backgroundColor(Color.TRANSPARENT)
            .cornerRadiusBottom(userConsentDialogStyle.getAllCornerRadius())
            .apply()

        disagreeBottomLinear.background =  ConsentDrawableVector.setupShape(context)
            .backgroundColor(Color.TRANSPARENT)
            .cornerRadius(userConsentDialogStyle.getAllCornerRadius())
            .apply()

        // TODO buttons
        val buttonParents =
            arrayListOf(euConsentBtnPersonalizedAgree, disagreeBtnNonPersonalizedAgree)
        for (buttonParent in buttonParents) {
            buttonParent.setTextColor(userConsentDialogStyle.getButtonTextColor())
            if (userConsentDialogStyle.isButtonMultipleColors()) {
                buttonParent.background = ConsentDrawableVector.setupShape(context)
                    .backgroundColors(userConsentDialogStyle.getButtonBackgroundColors())
                    .cornerRadius(userConsentDialogStyle.getButtonCornerRadius())
                    .apply()

            } else {
                buttonParent.background = ConsentDrawableVector.setupShape(context)
                    .backgroundColor(userConsentDialogStyle.getButtonBackgroundColor())
                    .cornerRadius(userConsentDialogStyle.getButtonCornerRadius())
                    .apply()
            }
        }

        if (userConsentDialogStyle.isTopBarDisplayIcon()) {
            euConsentIcon.visibility = View.VISIBLE
            providerIcon.visibility = View.VISIBLE
            disagreeIcon.visibility = View.VISIBLE

            euConsentIcon.setImageDrawable(getAppIconURIString(context))
            providerIcon.setImageDrawable(getAppIconURIString(context))
            disagreeIcon.setImageDrawable(getAppIconURIString(context))
        }

        euConsentBtnPersonalizedAgree.text = userConsentDialogStyle.getButtonPersonalizedIAgreeText()
        disagreeBtnNonPersonalizedAgree.text = userConsentDialogStyle.getButtonPersonalizedAgreeText()
        euConsentBtnNoThanks.text = userConsentDialogStyle.getButtonPersonalizedDisagreeText()

        ConsentTextSpannable.makeLinks(
            euConsentChangeSetting!!,
            arrayOf(dataIsUsed),
            arrayOf(dataIsUsedClickSpan)
        )

        rootView.addView(layoutEuConsent, rootViewParams)

        when (windowMode) {
            WindowMode.DIALOG -> {
                dialog = AppCompatDialog(context, R.style.user_consent_DialogTheme)
                dialog?.setContentView(rootView)
                dialog?.window!!.setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                dialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                // dialog.window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) //FLAG_DIM_BEHIND
                //dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                dialog?.setCancelable(false)
                if (isShowingWindowWithAnimation) {
                    dialog?.window!!.attributes.windowAnimations = R.style.user_consent_DialogAnimation
                }
                dialog?.show()
            }

            WindowMode.SHEET_BOTTOM -> {
                dialog = BottomSheetDialog(context!!) // , R.style.BottomSheetDialogTheme
                dialog?.setCancelable(false)
                dialog?.setCanceledOnTouchOutside(false)
                dialog?.setContentView(rootView)
                dialog?.setOnShowListener { dialog ->
                    handler.post {
                        val d = dialog as BottomSheetDialog
                        val touchOutsideView: View = d.window!!.decorView.findViewById(R.id.touch_outside)
                        touchOutsideView.setOnClickListener(null)

                        val bottomSheet = d.findViewById<FrameLayout>(R.id.design_bottom_sheet)
                        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet!!)
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                        bottomSheetBehavior.peekHeight = 0
                        bottomSheetBehavior.isHideable = false
                        bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetCallback() {
                            override fun onStateChanged(@NonNull bottomSheet: View, newState: Int) {
                                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                                }
                            }

                            override fun onSlide(@NonNull bottomSheet: View, slideOffset: Float) {}
                        })
                    }
                }
                dialog?.show()
            }
        }

        euConsentBtnNoThanks.setOnClickListener {
            //dialog?.setContentView(layoutDisagree)
            rootView.removeAllViews().also {
                rootView.post {
                    rootView.addView(layoutDisagree, rootViewParams)
                }
            }
        }
        providerImgBack.setOnClickListener {
            rootView.removeAllViews().also {
                rootView.post {
                    rootView.addView(layoutEuConsent, rootViewParams)
                }
            }
        }
        disagreeImgBack.setOnClickListener {
            rootView.removeAllViews().also {
                rootView.post {
                    rootView.addView(layoutEuConsent, rootViewParams)
                }
            }
            //dialog?.setContentView(layoutEuConsent)
            //dialog?.show()
        }
    }

    inner class UserConsentButtonsListener(private var userConsentEventListener: UserConsentEventListener?) :
        View.OnClickListener {
        override fun onClick(v: View?) {
            when (v!!.id) {
                R.id.eu_consent_personalized_agree -> {
                    ConsentInformation.getInstance(context).consentStatus =
                        ConsentStatus.PERSONALIZED
                    userConsentPreferences.addUserConsentInformation(ConsentStatus.PERSONALIZED.name)
                    userConsentPreferences.setUserConsentPersonalizedAdPref(userConsentPreferences.isPersonalized)
                    userConsentPreferences.setUserConsentPersonalizedDate(ConsentUtils.getCurrentDate())
                    userConsentEventListener?.onResult(ConsentStatus.PERSONALIZED, true)
                }
                R.id.disagree_btn_non_personalized_agree -> {
                    ConsentInformation.getInstance(context).consentStatus =
                        ConsentStatus.NON_PERSONALIZED
                    userConsentPreferences.addUserConsentInformation(ConsentStatus.NON_PERSONALIZED.name)
                    userConsentPreferences.setUserConsentPersonalizedAdPref(userConsentPreferences.nonPersonalized)
                    userConsentPreferences.setUserConsentNonPersonalizedDate(ConsentUtils.getCurrentDate())
                    userConsentEventListener?.onResult(ConsentStatus.NON_PERSONALIZED, false)
                }
            }
            if (dialog != null && dialog!!.isShowing) {
                dialog?.dismiss()
            }
        }
    }

    fun revokeUserConsentDialog() {
        val alertBuilder = AlertDialog.Builder(context)
        alertBuilder.setMessage(ResourcesCompat.getString(context, R.string.user_consent_dialog_msg_revoke_consent))
        alertBuilder.setPositiveButton(
            ResourcesCompat.getString(context, R.string.user_consent_yes)
        ) { dialog, _ ->
            dialog?.dismiss()
            dialog?.cancel()
            revokeUserConsent()
        }
        alertBuilder.setNegativeButton(
            ResourcesCompat.getString(context, R.string.user_consent_no)
        ) { dialog, _ ->
            dialog?.dismiss()
            dialog?.cancel()
        }

        val dialog = alertBuilder.create()
        dialog.window!!.attributes.windowAnimations = R.style.user_consent_DialogAnimation
        dialog.show()
    }

    fun revokeUserConsent() {
        ConsentInformation.getInstance(context).consentStatus = ConsentStatus.UNKNOWN
        userConsentPreferences.addUserConsentInformation(ConsentStatus.UNKNOWN.toString())
        userConsentPreferences.setUserConsentPersonalizedAdPref(userConsentPreferences.nonPersonalized)
        UserConsentPreferences.getInstance(context).setUserConfirmAgreement(false)
        Toast.makeText(
            context,
            ResourcesCompat.getString(context, R.string.user_consent_revoke_consent_success),
            Toast.LENGTH_LONG
        ).show()
    }

    fun updateUserConsent(consentStatus: ConsentStatus) {
        ConsentInformation.getInstance(context).consentStatus = consentStatus
        userConsentPreferences.addUserConsentInformation(consentStatus.toString())
        when (consentStatus) {
            ConsentStatus.PERSONALIZED -> {
                userConsentPreferences.updateUserConsentPersonalized(userConsentPreferences.isPersonalized)
                UserConsentPreferences.getInstance(context).setUserConfirmAgreement(true)
            }
            ConsentStatus.NON_PERSONALIZED -> {
                userConsentPreferences.updateUserConsentPersonalized(userConsentPreferences.nonPersonalized)
                UserConsentPreferences.getInstance(context).setUserConfirmAgreement(false)
            }
            else -> {
                userConsentPreferences.updateUserConsentPersonalized(userConsentPreferences.isPersonalized)
                UserConsentPreferences.getInstance(context).setUserConfirmAgreement(true)
            }
        }
        Toast.makeText(
            context,
            String.format(
                ResourcesCompat.getString(context, R.string.user_consent_user_accepts_ads),
                consentStatus.toString()
            ),
            Toast.LENGTH_LONG
        ).show()
    }

    private fun getAppName(context: Context?): String {
        return context!!.applicationInfo.loadLabel(context.packageManager).toString()
    }

    private fun getAppIconURIString(context: Context?): Drawable {
        return context!!.packageManager.getApplicationIcon(context.applicationInfo)
    }

    companion object {
        private var userConsent: UserConsentSdk? = null

        @Synchronized
        fun getInstance(context: Context): UserConsentSdk {
            synchronized(UserConsentSdk::class.java) {
                if (userConsent == null) {
                    return UserConsentSdk(context)
                }
            }
            return userConsent!!
        }
    }

}
