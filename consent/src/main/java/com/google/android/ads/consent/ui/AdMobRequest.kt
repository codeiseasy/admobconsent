package com.google.android.ads.consent.ui

import com.google.android.gms.ads.AdRequest

object AdMobRequest {
    const val DEVICE_ID_EMULATOR = AdRequest.DEVICE_ID_EMULATOR
}