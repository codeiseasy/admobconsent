package com.google.android.ads.consent.listener.enums;

public enum AdContentRating {
    G,
    PG,
    T,
    MA
}
