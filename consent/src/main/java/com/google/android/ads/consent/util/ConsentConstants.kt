package com.google.android.ads.consent.util

object ConsentConstants {
    const val extraKeyPrivacyUrl = "extra_key_privacy_url"
    const val extraKeyPublisherId = "extra_key_publisher_id"
}
