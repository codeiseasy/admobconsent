package com.google.android.ads.consent.util

import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat

object ResourcesCompat {
    fun getString(context: Context?, @StringRes res: Int): String {
        return context!!.resources.getString(res)
    }

    fun getColor(context: Context?, colorRes: Int): Int {
        return ContextCompat.getColor(context!!, colorRes)
    }

    fun getDrawable(context: Context?, colorRes: Int): Drawable? {
        return ContextCompat.getDrawable(context!!, colorRes)
    }

    fun getFont(context: Context?, fontRes: Int): Typeface? {
        return ResourcesCompat.getFont(context!!, fontRes)
    }
}