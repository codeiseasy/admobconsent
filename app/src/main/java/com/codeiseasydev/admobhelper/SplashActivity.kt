package com.codeiseasydev.admobhelper

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.android.ads.consent.ConsentStatus
import com.google.android.ads.consent.UserConsentSdk

import com.google.android.ads.consent.enums.WindowMode
import com.google.android.ads.consent.listener.interfaces.UserConsentEventListener
import com.google.android.ads.consent.prefs.UserConsentPreferences
import com.google.android.ads.consent.ui.UserConsentDialogStyle

class SplashActivity : UserConsentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (UserConsentPreferences.getInstance(this@SplashActivity).isUserConfirmAgreement()){
             goToMain()
        } else {
            checkUserConsent(object : UserConsentEventListener {
                override fun onResult(consentStatus: ConsentStatus, isRequestLocationInEeaOrUnknown: Boolean) {
                    UserConsentPreferences.getInstance(this@SplashActivity).setUserConfirmAgreement(true)
                    goToMain()
                    // Integration AppLovin
                    //AppLovinPrivacySettings.setHasUserConsent(isRequestLocationInEeaOrUnknown,  this@SplashActivity)
                    //AppLovinPrivacySettings.setIsAgeRestrictedUser(true, this@SplashActivity)
                }

                override fun onFailed(reason: String) {
                    UserConsentPreferences.getInstance(this@SplashActivity).setUserConfirmAgreement(true)
                    goToMain()
                }
            })
        }
    }

    fun goToMain(){
        startActivity(Intent(this@SplashActivity, AppMainActivity::class.java))
        finish()
    }
}