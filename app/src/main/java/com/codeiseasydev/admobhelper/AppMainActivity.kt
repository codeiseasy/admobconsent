package com.codeiseasydev.admobhelper

import android.os.Bundle
import android.util.TypedValue
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.codeiseasydev.admobhelper.Constants.privacyPolicy
import com.codeiseasydev.admobhelper.Constants.publisherId

import com.google.android.ads.consent.UserConsentSdk
import com.google.android.ads.consent.ui.UserConsentSettingsActivity

class AppMainActivity : AppCompatActivity() {
    //private var adMobHelper: AdMobHelper? = null
    private var btnSettingConsent: Button? = null
    private var btnRevokeConsent: Button? = null

    private fun dimension(value: Int): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            value.toFloat(),
            resources.displayMetrics
        )
    }

    private fun initViews(){
        btnSettingConsent = findViewById(R.id.btn_setting_consent)
        btnRevokeConsent = findViewById(R.id.btn_revoke_consent)

        /*btn_load_interstitial.setOnClickListener {
            adMobHelper?.loadInterstitialAfterEndCount(it.id.toString(), 1)
            return@setOnClickListener
        }
        btn_load_rewarded_video.setOnClickListener {
            adMobHelper?.loadRewardedVideoAfterEndCount(it.id.toString(), 1)
            return@setOnClickListener
        }*/
        btnSettingConsent?.setOnClickListener {
            UserConsentSettingsActivity.start(this, privacyPolicy, publisherId)
        }
        btnRevokeConsent?.setOnClickListener {
            UserConsentSdk.getInstance(this).revokeUserConsentDialog()
        }

    }

    private fun initAds(){
       /* adMobHelper = AdMobHelper.Builder()
            .setContext(this)
            .setDebugTestAds(true)
            .setAppUnitId(resources.getString(R.string.test_admob_app_unit_id))
            .setAdViewUnitId(resources.getString(R.string.test_admob_banner_unit_id))
            .setInterstitialUnitId(resources.getString(R.string.test_admob_interstitial_unit_id))
            .setRewardedVideoUnitId(resources.getString(R.string.test_admob_rewarded_video_unit_id))
            .setNativeAdvancedUnitId(resources.getString(R.string.test_admob_native_advance_unit_id))
            .setLoadAnimationBeforeLoadAd(true)
            .setLoadAnimationBeforeLoadAdAccentColor(R.color.colorPrimaryDark)
            .setAdViewSize(AdSize.BANNER)
            .setEnableAds(false)
            .setEnableBannerAd(true)
            .build()*/

        //adMobHelper?.loadAdView(unitAdView)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initAds()
        initViews()
    }

  /*  private fun loadApplovinBannerAd(){
       val adView = AppLovinAdView(AppLovinAdSize.BANNER, "YOUR_ZONE_ID",this)
       adView.id = ViewCompat.generateViewId()
       unitAdView.addView(adView)

        val isTablet = AppLovinSdkUtils.isTablet(this)
        val heightPx = AppLovinSdkUtils.dpToPx(this, if (isTablet) 90 else 50)
        adView.layoutParams = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, heightPx)

        // Load an ad!
        adView.loadNextAd()

        //
        // Optional: Set listeners
        //
        adView.setAdLoadListener(object : AppLovinAdLoadListener {
            override fun adReceived(ad: AppLovinAd) {
                LogDebug.d("APP_LOVIN_AD", "Banner loaded")
            }

            override fun failedToReceiveAd(errorCode: Int) {
                // Look at AppLovinErrorCodes.java for list of error codes
                LogDebug.d("APP_LOVIN_AD", "Banner failed to load with error code $errorCode")
            }
        })
    }*/

    override fun onPause() {
        super.onPause()
        //adMobHelper?.pause()
    }

    override fun onResume() {
        super.onResume()
        //adMobHelper?.resume()
    }

    override fun onDestroy() {
        //adMobHelper?.destroy()
        super.onDestroy()
    }
}
